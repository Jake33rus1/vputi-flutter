import 'package:flutter/material.dart';

class AutocompleteInputView extends StatelessWidget {
  const AutocompleteInputView({Key? key}) : super(key: key);

  static const List<String> _kOptions = <String>[
    'Казань',
    'Москва',
    'Санкт-Петербур'
  ];

  @override
  Widget build(BuildContext context) {
    return Autocomplete<String>(
      optionsBuilder: (TextEditingValue textEditingValue) {
        if (textEditingValue.text == '') {
          return const Iterable<String>.empty();
        }
        return _kOptions.where((String option) {
          return option.contains(textEditingValue.text.toLowerCase());
        });
      },
    );
  }
}
