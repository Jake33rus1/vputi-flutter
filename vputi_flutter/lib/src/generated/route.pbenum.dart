///
//  Generated code. Do not modify.
//  source: route.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Transport extends $pb.ProtobufEnum {
  static const Transport PEDESTRIAN = Transport._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PEDESTRIAN');
  static const Transport CAR = Transport._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CAR');
  static const Transport BUS = Transport._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'BUS');
  static const Transport BICYCLE = Transport._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'BICYCLE');

  static const $core.List<Transport> values = <Transport> [
    PEDESTRIAN,
    CAR,
    BUS,
    BICYCLE,
  ];

  static final $core.Map<$core.int, Transport> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Transport? valueOf($core.int value) => _byValue[value];

  const Transport._($core.int v, $core.String n) : super(v, n);
}

class GetRoutesResponse_Point_PointType extends $pb.ProtobufEnum {
  static const GetRoutesResponse_Point_PointType EVENT = GetRoutesResponse_Point_PointType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'EVENT');
  static const GetRoutesResponse_Point_PointType EAT = GetRoutesResponse_Point_PointType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'EAT');
  static const GetRoutesResponse_Point_PointType HOTEL = GetRoutesResponse_Point_PointType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'HOTEL');
  static const GetRoutesResponse_Point_PointType ROUTE_POINT = GetRoutesResponse_Point_PointType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ROUTE_POINT');

  static const $core.List<GetRoutesResponse_Point_PointType> values = <GetRoutesResponse_Point_PointType> [
    EVENT,
    EAT,
    HOTEL,
    ROUTE_POINT,
  ];

  static final $core.Map<$core.int, GetRoutesResponse_Point_PointType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static GetRoutesResponse_Point_PointType? valueOf($core.int value) => _byValue[value];

  const GetRoutesResponse_Point_PointType._($core.int v, $core.String n) : super(v, n);
}

