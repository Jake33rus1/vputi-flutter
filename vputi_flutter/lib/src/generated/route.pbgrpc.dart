///
//  Generated code. Do not modify.
//  source: route.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'route.pb.dart' as $0;
export 'route.pb.dart';

class RouteServiceClient extends $grpc.Client {
  static final _$getRoutes =
      $grpc.ClientMethod<$0.GetRoutesRequest, $0.GetRoutesResponse>(
          '/adventure.net.controller.RouteService/GetRoutes',
          ($0.GetRoutesRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetRoutesResponse.fromBuffer(value));

  RouteServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$0.GetRoutesResponse> getRoutes(
      $0.GetRoutesRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$getRoutes, $async.Stream.fromIterable([request]),
        options: options);
  }
}

abstract class RouteServiceBase extends $grpc.Service {
  $core.String get $name => 'adventure.net.controller.RouteService';

  RouteServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.GetRoutesRequest, $0.GetRoutesResponse>(
        'GetRoutes',
        getRoutes_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.GetRoutesRequest.fromBuffer(value),
        ($0.GetRoutesResponse value) => value.writeToBuffer()));
  }

  $async.Stream<$0.GetRoutesResponse> getRoutes_Pre($grpc.ServiceCall call,
      $async.Future<$0.GetRoutesRequest> request) async* {
    yield* getRoutes(call, await request);
  }

  $async.Stream<$0.GetRoutesResponse> getRoutes(
      $grpc.ServiceCall call, $0.GetRoutesRequest request);
}
