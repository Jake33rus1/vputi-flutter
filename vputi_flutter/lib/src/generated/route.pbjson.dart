///
//  Generated code. Do not modify.
//  source: route.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use transportDescriptor instead')
const Transport$json = const {
  '1': 'Transport',
  '2': const [
    const {'1': 'PEDESTRIAN', '2': 0},
    const {'1': 'CAR', '2': 1},
    const {'1': 'BUS', '2': 2},
    const {'1': 'BICYCLE', '2': 3},
  ],
};

/// Descriptor for `Transport`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List transportDescriptor = $convert.base64Decode('CglUcmFuc3BvcnQSDgoKUEVERVNUUklBThAAEgcKA0NBUhABEgcKA0JVUxACEgsKB0JJQ1lDTEUQAw==');
@$core.Deprecated('Use pointLocationDescriptor instead')
const PointLocation$json = const {
  '1': 'PointLocation',
  '2': const [
    const {'1': 'longitude', '3': 1, '4': 1, '5': 2, '10': 'longitude'},
    const {'1': 'latitude', '3': 2, '4': 1, '5': 2, '10': 'latitude'},
  ],
};

/// Descriptor for `PointLocation`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pointLocationDescriptor = $convert.base64Decode('Cg1Qb2ludExvY2F0aW9uEhwKCWxvbmdpdHVkZRgBIAEoAlIJbG9uZ2l0dWRlEhoKCGxhdGl0dWRlGAIgASgCUghsYXRpdHVkZQ==');
@$core.Deprecated('Use getRoutesRequestDescriptor instead')
const GetRoutesRequest$json = const {
  '1': 'GetRoutesRequest',
  '2': const [
    const {'1': 'from', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'from'},
    const {'1': 'to', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'to'},
    const {'1': 'fromLocation', '3': 3, '4': 1, '5': 11, '6': '.adventure.net.controller.PointLocation', '10': 'fromLocation'},
    const {'1': 'toLocation', '3': 4, '4': 1, '5': 11, '6': '.adventure.net.controller.PointLocation', '10': 'toLocation'},
    const {'1': 'prefer_transport', '3': 5, '4': 3, '5': 14, '6': '.adventure.net.controller.Transport', '10': 'preferTransport'},
    const {'1': 'point_count', '3': 6, '4': 1, '5': 5, '10': 'pointCount'},
    const {'1': 'eat_count', '3': 7, '4': 1, '5': 5, '10': 'eatCount'},
    const {'1': 'money', '3': 8, '4': 1, '5': 2, '10': 'money'},
    const {'1': 'use_hotel', '3': 10, '4': 1, '5': 8, '10': 'useHotel'},
    const {'1': 'city_id', '3': 11, '4': 1, '5': 3, '10': 'cityId'},
    const {'1': 'constraints', '3': 12, '4': 1, '5': 11, '6': '.adventure.net.controller.GetRoutesRequest.Constraints', '10': 'constraints'},
  ],
  '3': const [GetRoutesRequest_Constraints$json],
};

@$core.Deprecated('Use getRoutesRequestDescriptor instead')
const GetRoutesRequest_Constraints$json = const {
  '1': 'Constraints',
  '2': const [
    const {'1': 'points', '3': 1, '4': 3, '5': 11, '6': '.adventure.net.controller.GetRoutesRequest.Constraints.PointInfo', '10': 'points'},
  ],
  '3': const [GetRoutesRequest_Constraints_PointInfo$json],
};

@$core.Deprecated('Use getRoutesRequestDescriptor instead')
const GetRoutesRequest_Constraints_PointInfo$json = const {
  '1': 'PointInfo',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'from', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'from'},
    const {'1': 'to', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'to'},
  ],
};

/// Descriptor for `GetRoutesRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRoutesRequestDescriptor = $convert.base64Decode('ChBHZXRSb3V0ZXNSZXF1ZXN0Ei4KBGZyb20YASABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgRmcm9tEioKAnRvGAIgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFICdG8SSwoMZnJvbUxvY2F0aW9uGAMgASgLMicuYWR2ZW50dXJlLm5ldC5jb250cm9sbGVyLlBvaW50TG9jYXRpb25SDGZyb21Mb2NhdGlvbhJHCgp0b0xvY2F0aW9uGAQgASgLMicuYWR2ZW50dXJlLm5ldC5jb250cm9sbGVyLlBvaW50TG9jYXRpb25SCnRvTG9jYXRpb24STgoQcHJlZmVyX3RyYW5zcG9ydBgFIAMoDjIjLmFkdmVudHVyZS5uZXQuY29udHJvbGxlci5UcmFuc3BvcnRSD3ByZWZlclRyYW5zcG9ydBIfCgtwb2ludF9jb3VudBgGIAEoBVIKcG9pbnRDb3VudBIbCgllYXRfY291bnQYByABKAVSCGVhdENvdW50EhQKBW1vbmV5GAggASgCUgVtb25leRIbCgl1c2VfaG90ZWwYCiABKAhSCHVzZUhvdGVsEhcKB2NpdHlfaWQYCyABKANSBmNpdHlJZBJYCgtjb25zdHJhaW50cxgMIAEoCzI2LmFkdmVudHVyZS5uZXQuY29udHJvbGxlci5HZXRSb3V0ZXNSZXF1ZXN0LkNvbnN0cmFpbnRzUgtjb25zdHJhaW50cxrgAQoLQ29uc3RyYWludHMSWAoGcG9pbnRzGAEgAygLMkAuYWR2ZW50dXJlLm5ldC5jb250cm9sbGVyLkdldFJvdXRlc1JlcXVlc3QuQ29uc3RyYWludHMuUG9pbnRJbmZvUgZwb2ludHMadwoJUG9pbnRJbmZvEg4KAmlkGAEgASgDUgJpZBIuCgRmcm9tGAIgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcFIEZnJvbRIqCgJ0bxgDIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSAnRv');
@$core.Deprecated('Use getRoutesResponseDescriptor instead')
const GetRoutesResponse$json = const {
  '1': 'GetRoutesResponse',
  '2': const [
    const {'1': 'start', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'start'},
    const {'1': 'end', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'end'},
    const {'1': 'total_price', '3': 3, '4': 1, '5': 2, '10': 'totalPrice'},
    const {'1': 'points', '3': 4, '4': 3, '5': 11, '6': '.adventure.net.controller.GetRoutesResponse.Point', '10': 'points'},
    const {'1': 'images', '3': 5, '4': 3, '5': 9, '10': 'images'},
  ],
  '3': const [GetRoutesResponse_Point$json],
};

@$core.Deprecated('Use getRoutesResponseDescriptor instead')
const GetRoutesResponse_Point$json = const {
  '1': 'Point',
  '2': const [
    const {'1': 'point_type', '3': 1, '4': 1, '5': 14, '6': '.adventure.net.controller.GetRoutesResponse.Point.PointType', '10': 'pointType'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'enter', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'enter'},
    const {'1': 'exit', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'exit'},
    const {'1': 'price', '3': 6, '4': 1, '5': 2, '10': 'price'},
    const {'1': 'location', '3': 7, '4': 1, '5': 11, '6': '.adventure.net.controller.PointLocation', '10': 'location'},
    const {'1': 'transport_type', '3': 8, '4': 1, '5': 14, '6': '.adventure.net.controller.Transport', '10': 'transportType'},
    const {'1': 'images', '3': 9, '4': 3, '5': 9, '10': 'images'},
  ],
  '4': const [GetRoutesResponse_Point_PointType$json],
};

@$core.Deprecated('Use getRoutesResponseDescriptor instead')
const GetRoutesResponse_Point_PointType$json = const {
  '1': 'PointType',
  '2': const [
    const {'1': 'EVENT', '2': 0},
    const {'1': 'EAT', '2': 1},
    const {'1': 'HOTEL', '2': 2},
    const {'1': 'ROUTE_POINT', '2': 3},
  ],
};

/// Descriptor for `GetRoutesResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRoutesResponseDescriptor = $convert.base64Decode('ChFHZXRSb3V0ZXNSZXNwb25zZRIwCgVzdGFydBgBIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSBXN0YXJ0EiwKA2VuZBgCIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSA2VuZBIfCgt0b3RhbF9wcmljZRgDIAEoAlIKdG90YWxQcmljZRJJCgZwb2ludHMYBCADKAsyMS5hZHZlbnR1cmUubmV0LmNvbnRyb2xsZXIuR2V0Um91dGVzUmVzcG9uc2UuUG9pbnRSBnBvaW50cxIWCgZpbWFnZXMYBSADKAlSBmltYWdlcxr3AwoFUG9pbnQSWgoKcG9pbnRfdHlwZRgBIAEoDjI7LmFkdmVudHVyZS5uZXQuY29udHJvbGxlci5HZXRSb3V0ZXNSZXNwb25zZS5Qb2ludC5Qb2ludFR5cGVSCXBvaW50VHlwZRISCgRuYW1lGAIgASgJUgRuYW1lEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbhIwCgVlbnRlchgEIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3RhbXBSBWVudGVyEi4KBGV4aXQYBSABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wUgRleGl0EhQKBXByaWNlGAYgASgCUgVwcmljZRJDCghsb2NhdGlvbhgHIAEoCzInLmFkdmVudHVyZS5uZXQuY29udHJvbGxlci5Qb2ludExvY2F0aW9uUghsb2NhdGlvbhJKCg50cmFuc3BvcnRfdHlwZRgIIAEoDjIjLmFkdmVudHVyZS5uZXQuY29udHJvbGxlci5UcmFuc3BvcnRSDXRyYW5zcG9ydFR5cGUSFgoGaW1hZ2VzGAkgAygJUgZpbWFnZXMiOwoJUG9pbnRUeXBlEgkKBUVWRU5UEAASBwoDRUFUEAESCQoFSE9URUwQAhIPCgtST1VURV9QT0lOVBAD');
