///
//  Generated code. Do not modify.
//  source: route.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'google/protobuf/timestamp.pb.dart' as $1;

import 'route.pbenum.dart';

export 'route.pbenum.dart';

class PointLocation extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PointLocation', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'adventure.net.controller'), createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'longitude', $pb.PbFieldType.OF)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latitude', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  PointLocation._() : super();
  factory PointLocation({
    $core.double? longitude,
    $core.double? latitude,
  }) {
    final _result = create();
    if (longitude != null) {
      _result.longitude = longitude;
    }
    if (latitude != null) {
      _result.latitude = latitude;
    }
    return _result;
  }
  factory PointLocation.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PointLocation.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PointLocation clone() => PointLocation()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PointLocation copyWith(void Function(PointLocation) updates) => super.copyWith((message) => updates(message as PointLocation)) as PointLocation; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PointLocation create() => PointLocation._();
  PointLocation createEmptyInstance() => create();
  static $pb.PbList<PointLocation> createRepeated() => $pb.PbList<PointLocation>();
  @$core.pragma('dart2js:noInline')
  static PointLocation getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PointLocation>(create);
  static PointLocation? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get longitude => $_getN(0);
  @$pb.TagNumber(1)
  set longitude($core.double v) { $_setFloat(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLongitude() => $_has(0);
  @$pb.TagNumber(1)
  void clearLongitude() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get latitude => $_getN(1);
  @$pb.TagNumber(2)
  set latitude($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLatitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearLatitude() => clearField(2);
}

class GetRoutesRequest_Constraints_PointInfo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRoutesRequest.Constraints.PointInfo', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'adventure.net.controller'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id')
    ..aOM<$1.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'from', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'to', subBuilder: $1.Timestamp.create)
    ..hasRequiredFields = false
  ;

  GetRoutesRequest_Constraints_PointInfo._() : super();
  factory GetRoutesRequest_Constraints_PointInfo({
    $fixnum.Int64? id,
    $1.Timestamp? from,
    $1.Timestamp? to,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    if (from != null) {
      _result.from = from;
    }
    if (to != null) {
      _result.to = to;
    }
    return _result;
  }
  factory GetRoutesRequest_Constraints_PointInfo.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRoutesRequest_Constraints_PointInfo.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRoutesRequest_Constraints_PointInfo clone() => GetRoutesRequest_Constraints_PointInfo()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRoutesRequest_Constraints_PointInfo copyWith(void Function(GetRoutesRequest_Constraints_PointInfo) updates) => super.copyWith((message) => updates(message as GetRoutesRequest_Constraints_PointInfo)) as GetRoutesRequest_Constraints_PointInfo; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRoutesRequest_Constraints_PointInfo create() => GetRoutesRequest_Constraints_PointInfo._();
  GetRoutesRequest_Constraints_PointInfo createEmptyInstance() => create();
  static $pb.PbList<GetRoutesRequest_Constraints_PointInfo> createRepeated() => $pb.PbList<GetRoutesRequest_Constraints_PointInfo>();
  @$core.pragma('dart2js:noInline')
  static GetRoutesRequest_Constraints_PointInfo getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRoutesRequest_Constraints_PointInfo>(create);
  static GetRoutesRequest_Constraints_PointInfo? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $1.Timestamp get from => $_getN(1);
  @$pb.TagNumber(2)
  set from($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasFrom() => $_has(1);
  @$pb.TagNumber(2)
  void clearFrom() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureFrom() => $_ensure(1);

  @$pb.TagNumber(3)
  $1.Timestamp get to => $_getN(2);
  @$pb.TagNumber(3)
  set to($1.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasTo() => $_has(2);
  @$pb.TagNumber(3)
  void clearTo() => clearField(3);
  @$pb.TagNumber(3)
  $1.Timestamp ensureTo() => $_ensure(2);
}

class GetRoutesRequest_Constraints extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRoutesRequest.Constraints', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'adventure.net.controller'), createEmptyInstance: create)
    ..pc<GetRoutesRequest_Constraints_PointInfo>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'points', $pb.PbFieldType.PM, subBuilder: GetRoutesRequest_Constraints_PointInfo.create)
    ..hasRequiredFields = false
  ;

  GetRoutesRequest_Constraints._() : super();
  factory GetRoutesRequest_Constraints({
    $core.Iterable<GetRoutesRequest_Constraints_PointInfo>? points,
  }) {
    final _result = create();
    if (points != null) {
      _result.points.addAll(points);
    }
    return _result;
  }
  factory GetRoutesRequest_Constraints.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRoutesRequest_Constraints.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRoutesRequest_Constraints clone() => GetRoutesRequest_Constraints()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRoutesRequest_Constraints copyWith(void Function(GetRoutesRequest_Constraints) updates) => super.copyWith((message) => updates(message as GetRoutesRequest_Constraints)) as GetRoutesRequest_Constraints; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRoutesRequest_Constraints create() => GetRoutesRequest_Constraints._();
  GetRoutesRequest_Constraints createEmptyInstance() => create();
  static $pb.PbList<GetRoutesRequest_Constraints> createRepeated() => $pb.PbList<GetRoutesRequest_Constraints>();
  @$core.pragma('dart2js:noInline')
  static GetRoutesRequest_Constraints getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRoutesRequest_Constraints>(create);
  static GetRoutesRequest_Constraints? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<GetRoutesRequest_Constraints_PointInfo> get points => $_getList(0);
}

class GetRoutesRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRoutesRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'adventure.net.controller'), createEmptyInstance: create)
    ..aOM<$1.Timestamp>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'from', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'to', subBuilder: $1.Timestamp.create)
    ..aOM<PointLocation>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fromLocation', protoName: 'fromLocation', subBuilder: PointLocation.create)
    ..aOM<PointLocation>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'toLocation', protoName: 'toLocation', subBuilder: PointLocation.create)
    ..pc<Transport>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'preferTransport', $pb.PbFieldType.PE, valueOf: Transport.valueOf, enumValues: Transport.values)
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pointCount', $pb.PbFieldType.O3)
    ..a<$core.int>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'eatCount', $pb.PbFieldType.O3)
    ..a<$core.double>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'money', $pb.PbFieldType.OF)
    ..aOB(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'useHotel')
    ..aInt64(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'cityId')
    ..aOM<GetRoutesRequest_Constraints>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'constraints', subBuilder: GetRoutesRequest_Constraints.create)
    ..hasRequiredFields = false
  ;

  GetRoutesRequest._() : super();
  factory GetRoutesRequest({
    $1.Timestamp? from,
    $1.Timestamp? to,
    PointLocation? fromLocation,
    PointLocation? toLocation,
    $core.Iterable<Transport>? preferTransport,
    $core.int? pointCount,
    $core.int? eatCount,
    $core.double? money,
    $core.bool? useHotel,
    $fixnum.Int64? cityId,
    GetRoutesRequest_Constraints? constraints,
  }) {
    final _result = create();
    if (from != null) {
      _result.from = from;
    }
    if (to != null) {
      _result.to = to;
    }
    if (fromLocation != null) {
      _result.fromLocation = fromLocation;
    }
    if (toLocation != null) {
      _result.toLocation = toLocation;
    }
    if (preferTransport != null) {
      _result.preferTransport.addAll(preferTransport);
    }
    if (pointCount != null) {
      _result.pointCount = pointCount;
    }
    if (eatCount != null) {
      _result.eatCount = eatCount;
    }
    if (money != null) {
      _result.money = money;
    }
    if (useHotel != null) {
      _result.useHotel = useHotel;
    }
    if (cityId != null) {
      _result.cityId = cityId;
    }
    if (constraints != null) {
      _result.constraints = constraints;
    }
    return _result;
  }
  factory GetRoutesRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRoutesRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRoutesRequest clone() => GetRoutesRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRoutesRequest copyWith(void Function(GetRoutesRequest) updates) => super.copyWith((message) => updates(message as GetRoutesRequest)) as GetRoutesRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRoutesRequest create() => GetRoutesRequest._();
  GetRoutesRequest createEmptyInstance() => create();
  static $pb.PbList<GetRoutesRequest> createRepeated() => $pb.PbList<GetRoutesRequest>();
  @$core.pragma('dart2js:noInline')
  static GetRoutesRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRoutesRequest>(create);
  static GetRoutesRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $1.Timestamp get from => $_getN(0);
  @$pb.TagNumber(1)
  set from($1.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasFrom() => $_has(0);
  @$pb.TagNumber(1)
  void clearFrom() => clearField(1);
  @$pb.TagNumber(1)
  $1.Timestamp ensureFrom() => $_ensure(0);

  @$pb.TagNumber(2)
  $1.Timestamp get to => $_getN(1);
  @$pb.TagNumber(2)
  set to($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasTo() => $_has(1);
  @$pb.TagNumber(2)
  void clearTo() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureTo() => $_ensure(1);

  @$pb.TagNumber(3)
  PointLocation get fromLocation => $_getN(2);
  @$pb.TagNumber(3)
  set fromLocation(PointLocation v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasFromLocation() => $_has(2);
  @$pb.TagNumber(3)
  void clearFromLocation() => clearField(3);
  @$pb.TagNumber(3)
  PointLocation ensureFromLocation() => $_ensure(2);

  @$pb.TagNumber(4)
  PointLocation get toLocation => $_getN(3);
  @$pb.TagNumber(4)
  set toLocation(PointLocation v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasToLocation() => $_has(3);
  @$pb.TagNumber(4)
  void clearToLocation() => clearField(4);
  @$pb.TagNumber(4)
  PointLocation ensureToLocation() => $_ensure(3);

  @$pb.TagNumber(5)
  $core.List<Transport> get preferTransport => $_getList(4);

  @$pb.TagNumber(6)
  $core.int get pointCount => $_getIZ(5);
  @$pb.TagNumber(6)
  set pointCount($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPointCount() => $_has(5);
  @$pb.TagNumber(6)
  void clearPointCount() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get eatCount => $_getIZ(6);
  @$pb.TagNumber(7)
  set eatCount($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasEatCount() => $_has(6);
  @$pb.TagNumber(7)
  void clearEatCount() => clearField(7);

  @$pb.TagNumber(8)
  $core.double get money => $_getN(7);
  @$pb.TagNumber(8)
  set money($core.double v) { $_setFloat(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasMoney() => $_has(7);
  @$pb.TagNumber(8)
  void clearMoney() => clearField(8);

  @$pb.TagNumber(10)
  $core.bool get useHotel => $_getBF(8);
  @$pb.TagNumber(10)
  set useHotel($core.bool v) { $_setBool(8, v); }
  @$pb.TagNumber(10)
  $core.bool hasUseHotel() => $_has(8);
  @$pb.TagNumber(10)
  void clearUseHotel() => clearField(10);

  @$pb.TagNumber(11)
  $fixnum.Int64 get cityId => $_getI64(9);
  @$pb.TagNumber(11)
  set cityId($fixnum.Int64 v) { $_setInt64(9, v); }
  @$pb.TagNumber(11)
  $core.bool hasCityId() => $_has(9);
  @$pb.TagNumber(11)
  void clearCityId() => clearField(11);

  @$pb.TagNumber(12)
  GetRoutesRequest_Constraints get constraints => $_getN(10);
  @$pb.TagNumber(12)
  set constraints(GetRoutesRequest_Constraints v) { setField(12, v); }
  @$pb.TagNumber(12)
  $core.bool hasConstraints() => $_has(10);
  @$pb.TagNumber(12)
  void clearConstraints() => clearField(12);
  @$pb.TagNumber(12)
  GetRoutesRequest_Constraints ensureConstraints() => $_ensure(10);
}

class GetRoutesResponse_Point extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRoutesResponse.Point', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'adventure.net.controller'), createEmptyInstance: create)
    ..e<GetRoutesResponse_Point_PointType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'pointType', $pb.PbFieldType.OE, defaultOrMaker: GetRoutesResponse_Point_PointType.EVENT, valueOf: GetRoutesResponse_Point_PointType.valueOf, enumValues: GetRoutesResponse_Point_PointType.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..aOM<$1.Timestamp>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'enter', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'exit', subBuilder: $1.Timestamp.create)
    ..a<$core.double>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'price', $pb.PbFieldType.OF)
    ..aOM<PointLocation>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'location', subBuilder: PointLocation.create)
    ..e<Transport>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'transportType', $pb.PbFieldType.OE, defaultOrMaker: Transport.PEDESTRIAN, valueOf: Transport.valueOf, enumValues: Transport.values)
    ..pPS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'images')
    ..hasRequiredFields = false
  ;

  GetRoutesResponse_Point._() : super();
  factory GetRoutesResponse_Point({
    GetRoutesResponse_Point_PointType? pointType,
    $core.String? name,
    $core.String? description,
    $1.Timestamp? enter,
    $1.Timestamp? exit,
    $core.double? price,
    PointLocation? location,
    Transport? transportType,
    $core.Iterable<$core.String>? images,
  }) {
    final _result = create();
    if (pointType != null) {
      _result.pointType = pointType;
    }
    if (name != null) {
      _result.name = name;
    }
    if (description != null) {
      _result.description = description;
    }
    if (enter != null) {
      _result.enter = enter;
    }
    if (exit != null) {
      _result.exit = exit;
    }
    if (price != null) {
      _result.price = price;
    }
    if (location != null) {
      _result.location = location;
    }
    if (transportType != null) {
      _result.transportType = transportType;
    }
    if (images != null) {
      _result.images.addAll(images);
    }
    return _result;
  }
  factory GetRoutesResponse_Point.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRoutesResponse_Point.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRoutesResponse_Point clone() => GetRoutesResponse_Point()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRoutesResponse_Point copyWith(void Function(GetRoutesResponse_Point) updates) => super.copyWith((message) => updates(message as GetRoutesResponse_Point)) as GetRoutesResponse_Point; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRoutesResponse_Point create() => GetRoutesResponse_Point._();
  GetRoutesResponse_Point createEmptyInstance() => create();
  static $pb.PbList<GetRoutesResponse_Point> createRepeated() => $pb.PbList<GetRoutesResponse_Point>();
  @$core.pragma('dart2js:noInline')
  static GetRoutesResponse_Point getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRoutesResponse_Point>(create);
  static GetRoutesResponse_Point? _defaultInstance;

  @$pb.TagNumber(1)
  GetRoutesResponse_Point_PointType get pointType => $_getN(0);
  @$pb.TagNumber(1)
  set pointType(GetRoutesResponse_Point_PointType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasPointType() => $_has(0);
  @$pb.TagNumber(1)
  void clearPointType() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $1.Timestamp get enter => $_getN(3);
  @$pb.TagNumber(4)
  set enter($1.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasEnter() => $_has(3);
  @$pb.TagNumber(4)
  void clearEnter() => clearField(4);
  @$pb.TagNumber(4)
  $1.Timestamp ensureEnter() => $_ensure(3);

  @$pb.TagNumber(5)
  $1.Timestamp get exit => $_getN(4);
  @$pb.TagNumber(5)
  set exit($1.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasExit() => $_has(4);
  @$pb.TagNumber(5)
  void clearExit() => clearField(5);
  @$pb.TagNumber(5)
  $1.Timestamp ensureExit() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.double get price => $_getN(5);
  @$pb.TagNumber(6)
  set price($core.double v) { $_setFloat(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPrice() => $_has(5);
  @$pb.TagNumber(6)
  void clearPrice() => clearField(6);

  @$pb.TagNumber(7)
  PointLocation get location => $_getN(6);
  @$pb.TagNumber(7)
  set location(PointLocation v) { setField(7, v); }
  @$pb.TagNumber(7)
  $core.bool hasLocation() => $_has(6);
  @$pb.TagNumber(7)
  void clearLocation() => clearField(7);
  @$pb.TagNumber(7)
  PointLocation ensureLocation() => $_ensure(6);

  @$pb.TagNumber(8)
  Transport get transportType => $_getN(7);
  @$pb.TagNumber(8)
  set transportType(Transport v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasTransportType() => $_has(7);
  @$pb.TagNumber(8)
  void clearTransportType() => clearField(8);

  @$pb.TagNumber(9)
  $core.List<$core.String> get images => $_getList(8);
}

class GetRoutesResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRoutesResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'adventure.net.controller'), createEmptyInstance: create)
    ..aOM<$1.Timestamp>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'start', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'end', subBuilder: $1.Timestamp.create)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'totalPrice', $pb.PbFieldType.OF)
    ..pc<GetRoutesResponse_Point>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'points', $pb.PbFieldType.PM, subBuilder: GetRoutesResponse_Point.create)
    ..pPS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'images')
    ..hasRequiredFields = false
  ;

  GetRoutesResponse._() : super();
  factory GetRoutesResponse({
    $1.Timestamp? start,
    $1.Timestamp? end,
    $core.double? totalPrice,
    $core.Iterable<GetRoutesResponse_Point>? points,
    $core.Iterable<$core.String>? images,
  }) {
    final _result = create();
    if (start != null) {
      _result.start = start;
    }
    if (end != null) {
      _result.end = end;
    }
    if (totalPrice != null) {
      _result.totalPrice = totalPrice;
    }
    if (points != null) {
      _result.points.addAll(points);
    }
    if (images != null) {
      _result.images.addAll(images);
    }
    return _result;
  }
  factory GetRoutesResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRoutesResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRoutesResponse clone() => GetRoutesResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRoutesResponse copyWith(void Function(GetRoutesResponse) updates) => super.copyWith((message) => updates(message as GetRoutesResponse)) as GetRoutesResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRoutesResponse create() => GetRoutesResponse._();
  GetRoutesResponse createEmptyInstance() => create();
  static $pb.PbList<GetRoutesResponse> createRepeated() => $pb.PbList<GetRoutesResponse>();
  @$core.pragma('dart2js:noInline')
  static GetRoutesResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRoutesResponse>(create);
  static GetRoutesResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $1.Timestamp get start => $_getN(0);
  @$pb.TagNumber(1)
  set start($1.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStart() => $_has(0);
  @$pb.TagNumber(1)
  void clearStart() => clearField(1);
  @$pb.TagNumber(1)
  $1.Timestamp ensureStart() => $_ensure(0);

  @$pb.TagNumber(2)
  $1.Timestamp get end => $_getN(1);
  @$pb.TagNumber(2)
  set end($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearEnd() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureEnd() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.double get totalPrice => $_getN(2);
  @$pb.TagNumber(3)
  set totalPrice($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTotalPrice() => $_has(2);
  @$pb.TagNumber(3)
  void clearTotalPrice() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<GetRoutesResponse_Point> get points => $_getList(3);

  @$pb.TagNumber(5)
  $core.List<$core.String> get images => $_getList(4);
}

