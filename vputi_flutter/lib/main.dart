import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:vputi_flutter/main_view/main_screen_page.dart';
import 'package:vputi_flutter/route_info/view/route_info_view.dart';
import 'package:vputi_flutter/select_interests_view/view/select_interests_view.dart';
import 'package:vputi_flutter/select_tour/select_tour_view.dart';
import 'package:vputi_flutter/utils/hex_color.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
        colorScheme:
            const ColorScheme.light().copyWith(primary: HexColor("FFFFFF")),
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('ru', 'RU'),
      ],
      locale: Locale('ru'),
      initialRoute: '/date_picker',
      routes: {
        '/': (context) => MainScreenPage(),
        '/select_interests': (context) => const SelectInterestsView(
              title: 'Укажите ваши интересны',
            ),
        '/select_route': (context) => SelectTourView(),
        '/user_routes': (context) => MainScreenPage(),
        '/route_info': (context) => RouteInfoView(),
        '/settings': (context) => MainScreenPage(),
        '/route_map': (context) => MainScreenPage(),
        '/register': (context) => MainScreenPage(),
        '/authorization': (context) => MainScreenPage(),
        '/date_picker': (context) => MainScreenPage(),
        '/password_recovery': (context) => MainScreenPage()
      },
    ));
