import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:vputi_flutter/utils/hex_color.dart';

class SelectDateView extends StatefulWidget {
  @override
  _SelectDateViewState createState() => _SelectDateViewState();
}

class _SelectDateViewState extends State<SelectDateView> {
  late DateTime _currentDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: Stack(
          children: [
            Positioned(child: buildCalendar()),
          ],
        ),
      ),
    );
  }

  Widget buildCalendar() => Container(
        margin: EdgeInsets.symmetric(horizontal: 16.0),
        child: CalendarCarousel<Event>(
          onDayPressed: (DateTime date, List<Event> events) {
            this.setState(() => _currentDate = date);
          },
          weekendTextStyle: TextStyle(color: Colors.black),
          thisMonthDayBorderColor: Colors.grey,
          locale: "ru-RU",
          minSelectedDate: DateTime.now(),
        ),
      );
}
