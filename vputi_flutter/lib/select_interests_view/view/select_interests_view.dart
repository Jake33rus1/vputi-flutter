import 'package:flutter/material.dart';
import 'package:vputi_flutter/network/ApiFactory.dart';
import 'package:vputi_flutter/network/contracts/interests/GetInterestsResponse.dart';
import '../../utils/hex_color.dart';
import '../model/InterestModel.dart';
import '../../network/ApiFactory.dart';

class SelectInterestsView extends StatefulWidget {
  final String title;

  const SelectInterestsView({required this.title});

  @override
  _SelectInterestsViewState createState() {
    return _SelectInterestsViewState();
  }
}

class _SelectInterestsViewState extends State<SelectInterestsView> {
  Future<GetInterestsResponse>? _interest;

  @override
  void initState() {
    _interest = ApiFactory.createInterestsApi().get();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          top: false,
          bottom: false,
          child: Column(
            children: [
              buildTitleView(),
              Expanded(
                child: Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: [
                      Positioned.fill(child: buildListView()),
                      buildContinueButton(),
                    ]),
              ),
            ],
          ),
        ),
      );

  Widget buildContinueButton() => Container(
        padding: EdgeInsets.fromLTRB(22, 12, 22, 12),
        decoration: BoxDecoration(
          color: HexColor("F6F6F6"),
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(20), bottom: Radius.zero),
        ),
        child: SizedBox(
          height: 44.0,
          child: ElevatedButton(
            onPressed: () {},
            child: Center(
              child: Text('Сохранить'),
            ),
            style: ElevatedButton.styleFrom(primary: HexColor("#3735C2")),
          ),
        ),
      );

  Widget buildListView() => Container(
        padding: const EdgeInsets.fromLTRB(22, 24, 22, 44),
        decoration: BoxDecoration(
          color: HexColor("DFDFDF"),
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(20), bottom: Radius.zero),
        ),
        child: ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: [
            FutureBuilder<GetInterestsResponse>(
                future: _interest,
                builder: (BuildContext context,
                    AsyncSnapshot<GetInterestsResponse> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      return Text(snapshot.error.toString());
                    }
                    if (!snapshot.hasData) {
                      return Text("No data");
                    }

                    return ListView(
                        children: snapshot.data!.interests!
                            .map((e) => buildSingleCheckbox(
                                InterestModel(title: e.name, id: e.id)))
                            .toList(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true);
                  }
                  return Text('State: ${snapshot.connectionState}');
                }),
            Padding(padding: EdgeInsets.only(top: 30))
          ],
        ),
      );

  Widget buildTitleView() => Container(
        margin: EdgeInsets.all(45.0),
        child: Center(
          child: const Text(
            'Укажите ваши интересы',
            style: TextStyle(fontSize: 30),
          ),
        ),
      );

  Widget buildSingleCheckbox(InterestModel interest) => buildCheckbox(
      interest: interest,
      onClicked: () {
        setState(() {
          interest.value = !interest.value;
        });
      });

  Widget buildCheckbox({
    required InterestModel interest,
    required VoidCallback onClicked,
  }) =>
      SizedBox(
          height: 60.0,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: new InkWell(
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.only(right: 14)),
                  Checkbox(
                    value: interest.value,
                    onChanged: (value) => onClicked(),
                  ),
                  Text(
                    interest.title,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              onTap: onClicked,
            ),
          ));
}
