import 'package:meta/meta.dart';

class InterestModel {
  int id;
  String title;
  bool value;

  InterestModel({
    required this.title,
    required this.id,
    this.value = false,
  });
}