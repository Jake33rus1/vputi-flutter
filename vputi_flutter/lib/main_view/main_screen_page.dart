import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vputi_flutter/mediaQuery/size_helpers.dart';
import 'package:vputi_flutter/uikit/uikit.dart';
import 'package:vputi_flutter/utils/hex_color.dart';
import 'package:intl/intl.dart';

class MainScreenPage extends StatefulWidget {
  static const String routeName = '/mainScreen';

  @override
  _MainScreenPageState createState() => _MainScreenPageState();
}

class _MainScreenPageState extends State<MainScreenPage> {
  DateTime toDate = DateTime.now();
  DateTime fromDate = DateTime.now();
  bool showToDate = false;
  bool showFromDate = false;

  Future<DateTime> _selectDate(BuildContext context, bool isFirstDate) async {
    final selected = await showDatePicker(
        builder: (context, child) {
          return Theme(
              data: Theme.of(context).copyWith(
                  colorScheme: ColorScheme.light(
                      primary: HexColor("3735C2"),
                      onPrimary: Colors.white,
                      onSurface: Colors.black54)),
              child: child!);
        },
        context: context,
        initialDate: isFirstDate ? toDate : fromDate,
        firstDate: isFirstDate ? toDate : fromDate,
        lastDate: DateTime(2030),
        locale: Locale('ru', 'RU'));
    if (isFirstDate) {
      if (selected != null && selected != toDate) {
        setState(() {
          toDate = selected;
          fromDate = selected;
        });
      }
    } else {
      if (selected != null && selected != fromDate) {
        setState(() {
          fromDate = selected;
        });
      }
    }
    return isFirstDate ? toDate : fromDate;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            top: true,
            bottom: true,
            child: Container(
                color: Colors.white,
                width: displayWidth(context),
                padding: EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Padding(padding: EdgeInsets.only(top: 20)),
                    Center(
                      child: Text(
                        "Куда вы хотите отправиться",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 30)),
                    Expanded(
                        child: Container(
                            width: double.infinity,
                            padding: EdgeInsets.all(22.0),
                            decoration: BoxDecoration(
                                color: HexColor("#F4F4F4"),
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(20))),
                            child: Column(children: [
                              _getCard(),
                              SizedBox(
                                height: 12,
                              ),
                              _getBudgetCard(),
                              SizedBox(
                                height: 12,
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.7,
                                child: _getButtons(),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              _getSearchButton(context)
                            ])))
                  ],
                ))));
  }

  _getCard() {
    return SizedBox(
        width: displayWidth(context) * 0.7,
        child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(mainAxisSize: MainAxisSize.max, children: [
              Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextFormField(
                    decoration: InputDecoration.collapsed(hintText: "Откуда"),
                  )),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Divider(
                  height: 2.0,
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(16.0),
                  child: TextFormField(
                    decoration: InputDecoration.collapsed(hintText: "Куда"),
                  )),
            ])));
  }

  _getBudgetCard() {
    return SizedBox(
        width: displayWidth(context) * 0.7,
        child: Card(
          elevation: 5,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          child: Padding(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: MyInputTextView(
                key: Key("budget"),
                label: "Ваш бюджет на поедку",
              )),
        ));
  }

  _getButtons() {
    final ButtonStyle style = ElevatedButton.styleFrom(
        primary: Colors.white, onPrimary: Colors.grey, elevation: 5);
    return SizedBox(
        child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            child: Padding(
                padding: EdgeInsets.all(16),
                child: Column(children: [
                  Text("Выберите даты для путешествия:"),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                          child: ElevatedButton(
                            onPressed: () {
                              _selectDate(context, true);
                            },
                            child: Text(
                              "с ${DateFormat("dd.MM.yyyy").format(toDate)} г.",
                              style: TextStyle(color: Colors.black),
                            ),
                            style: style,
                          ),
                          height: 40.0,
                          width: MediaQuery.of(context).size.width * 0.3),
                      SizedBox(
                          child: ElevatedButton(
                            onPressed: () {
                              _selectDate(context, false);
                            },
                            child: Text(
                              "по ${DateFormat("dd.MM.yyyy").format(fromDate)} г.",
                              style: TextStyle(color: Colors.black),
                            ),
                            style: style,
                          ),
                          height: 40.0,
                          width: MediaQuery.of(context).size.width * 0.3),
                    ],
                  )
                ]))));
  }
}

_getSearchButton(BuildContext context) {
  return SizedBox(
    child: ElevatedButton(
      onPressed: () {},
      child: Text(
        "Найти".toUpperCase(),
        style: TextStyle(color: Colors.white),
      ),
      style: ElevatedButton.styleFrom(
          primary: HexColor("3735C2"), onPrimary: Colors.white30, elevation: 5),
    ),
    height: 40.0,
    width: displayWidth(context) * 0.7,
  );
}

class MyInputTextView extends StatelessWidget {
  final String label;

  MyInputTextView({
    required Key key,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 60,
        child: TextField(
          decoration: new InputDecoration(
              labelStyle: TextStyle(color: Colors.grey), labelText: label),
          keyboardType: TextInputType.number,
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
            CurrencyPtBrInputFormatter(maxDigits: 8)
          ],
        ));
  }
}

class CurrencyPtBrInputFormatter extends TextInputFormatter {
  CurrencyPtBrInputFormatter({required this.maxDigits});
  final int maxDigits;

  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    if (maxDigits != null && newValue.selection.baseOffset > maxDigits) {
      return oldValue;
    }

    double value = double.parse(newValue.text);
    final formatter = new NumberFormat("#,##0.00", "ru_RU");
    String newText = formatter.format(value / 100) + " ₽";
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}
