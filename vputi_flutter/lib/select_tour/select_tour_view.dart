import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class SelectTourView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SelectTourViewState();
}

class _SelectTourViewState extends State<SelectTourView> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Center(
                  child: Text(
                    "Выберите свой маршрут",
                    style: TextStyle(fontSize: 14),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [_getCard()],
            ),
          ],
        ),
      ),
    );
  }

  _getCard() {
    return SizedBox(
      height: 160,
      width: MediaQuery.of(context).size.width * 0.8,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(padding: EdgeInsets.only(top: 10)),
            RichText(
                text: TextSpan(
                    text: "Карелия. ",
                    style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),
                    children: [
                  TextSpan(
                      text: "Медвежьегорск-Петрозаводск",
                      style:
                          TextStyle(fontSize: 9, fontWeight: FontWeight.normal))
                ])),
            CarouselSlider(
              options: CarouselOptions(
                  height: 100,
                  scrollDirection: Axis.horizontal,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _currentIndex = index;
                    });
                  }),
              items: imagesList
                  .map(
                    (item) => Card(
                      margin: EdgeInsets.only(
                          top: 10.0, bottom: 10, left: 5, right: 5),
                      child: Stack(
                        children: [
                          Image.network(item,
                              fit: BoxFit.cover, width: double.infinity),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Text(
                      "Посетите места самого красивого края России.",
                      style: TextStyle(fontSize: 8),
                      textAlign: TextAlign.left,
                      softWrap: true,
                    ),
                  ),
                ),
                Flexible(
                    flex: 1,
                    child: Padding(
                      padding: EdgeInsets.only(right: 8),
                      child: Text(
                        "12 точек",
                        style: TextStyle(fontSize: 8),
                        textAlign: TextAlign.end,
                      ),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }

  _getImageSelector() {
    return imagesList.map((urlOfItem) {
      int index = imagesList.indexOf(urlOfItem);
      return Container(
        width: 10.0,
        height: 10.0,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: _currentIndex == index
              ? Color.fromRGBO(0, 0, 0, 0.8)
              : Color.fromRGBO(0, 0, 0, 0.3),
        ),
      );
    }).toList();
  }
}

final List<String> imagesList = [
  'https://images.unsplash.com/photo-1604612503444-4b6e7fc9964d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80',
  'https://images.unsplash.com/photo-1581354648746-69d44b529db6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
  'https://images.unsplash.com/photo-1632154030737-b15df986ee37?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1548&q=80',
  'https://images.unsplash.com/photo-1610796493548-b2c31d5c0e00?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
];
