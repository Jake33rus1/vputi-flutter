import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'contracts/location/FindCityRequest.dart';
import 'contracts/location/GetCitiesResponse.dart';
import 'contracts/location/GetTopCitiesResponse.dart';

part 'LocationApi.g.dart';

@RestApi()
abstract class LocationApi {
  factory LocationApi(Dio dio, {String baseUrl}) = _LocationApi;

  @GET("/cities/search")
  Future<GetCitiesResponse> findCities(@Queries() FindCityRequest request);

  @GET("/cities/top")
  Future<GetTopCitiesResponse> getTopCities();
}
