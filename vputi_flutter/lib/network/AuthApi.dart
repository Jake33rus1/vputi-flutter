import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vputi_flutter/network/contracts/auth/AuthTokenResponse.dart';
import 'package:vputi_flutter/network/contracts/auth/LoginExternalRequest.dart';
import 'package:vputi_flutter/network/contracts/auth/LoginPasswordRequest.dart';
import 'package:vputi_flutter/network/contracts/auth/RefreshTokenRequest.dart';
import 'package:vputi_flutter/network/contracts/auth/RegisterRequest.dart';

part 'AuthApi.g.dart';

@RestApi()
abstract class AuthApi {
  factory AuthApi(Dio dio, {String baseUrl}) = _AuthApi;

  @POST("/login")
  Future<AuthTokenResponse> login(@Body() LoginPasswordRequest request);

  @POST("/refresh")
  Future<AuthTokenResponse> refresh(@Body() RefreshTokenRequest request);

  @POST("/register")
  Future<AuthTokenResponse> register(@Body() RegisterRequest request);

  @POST("/signout")
  Future signout(@Header("Authorization") String token);
}
