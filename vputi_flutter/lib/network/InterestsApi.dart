import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'contracts/interests/GetInterestsResponse.dart';

part 'InterestsApi.g.dart';

@RestApi()
abstract class InterestsApi {
  factory InterestsApi(Dio dio, {String baseUrl}) = _InterestsApi;

  @GET("/")
  Future<GetInterestsResponse> get();
}
