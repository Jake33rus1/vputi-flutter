import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vputi_flutter/network/contracts/support/AddCityRequest.dart';

part 'SupportApi.g.dart';

@RestApi()
abstract class SupportApi {
  factory SupportApi(Dio dio, {String baseUrl}) = _SupportApi;

  @GET("/cities/add")
  Future addCity(@Queries() AddCityRequest request);
}
