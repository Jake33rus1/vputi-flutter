import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vputi_flutter/network/contracts/adventure/GetActualRouteResponse.dart';
import 'package:vputi_flutter/network/contracts/adventure/GetUserRouteResponse.dart';

import 'contracts/adventure/AddRouteRequest.dart';
import 'contracts/adventure/AddRouteResponse.dart';
import 'contracts/adventure/GetRouteResponse.dart';

part 'AdventureApi.g.dart';

@RestApi()
abstract class AdventureApi {
  factory AdventureApi(Dio dio, {String baseUrl}) = _AdventureApi;

  @GET("/routes")
  Future<GetUserRouteResponse> getUserRoutes();

  @GET("/routes/current")
  Future<GetActualRouteResponse> getActualRoute();

  @POST("/routes")
  Future<AddRouteResponse> addRoute(@Body() AddRouteRequest request);

  @GET("/routes/{id}")
  Future<GetRouteResponse> GetRoute(@Path("id") int id);
}
