import 'package:json_annotation/json_annotation.dart';

part 'GetInterestsResponse.g.dart';

@JsonSerializable()
class GetInterestsResponse {
  List<Interest>? interests;

  GetInterestsResponse({required this.interests});

  factory GetInterestsResponse.fromJson(Map<String, dynamic> json) =>
      _$GetInterestsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetInterestsResponseToJson(this);
}

@JsonSerializable()
class Interest {
  int id;
  String name;
  List<Interest>? interests;

  Interest({required this.id, required this.name, required this.interests});

  factory Interest.fromJson(Map<String, dynamic> json) =>
      _$InterestFromJson(json);

  Map<String, dynamic> toJson() => _$InterestToJson(this);
}
