// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetInterestsResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetInterestsResponse _$GetInterestsResponseFromJson(
        Map<String, dynamic> json) =>
    GetInterestsResponse(
      interests: (json['interests'] as List<dynamic>?)
          ?.map((e) => Interest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetInterestsResponseToJson(
        GetInterestsResponse instance) =>
    <String, dynamic>{
      'interests': instance.interests,
    };

Interest _$InterestFromJson(Map<String, dynamic> json) => Interest(
      id: json['id'] as int,
      name: json['name'] as String,
      interests: (json['interests'] as List<dynamic>?)
          ?.map((e) => Interest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$InterestToJson(Interest instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'interests': instance.interests,
    };
