// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LoginExternalRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginExternalRequest _$LoginExternalRequestFromJson(
        Map<String, dynamic> json) =>
    LoginExternalRequest(
      externalId: json['externalId'] as String,
      externalType: json['externalType'] as String,
    );

Map<String, dynamic> _$LoginExternalRequestToJson(
        LoginExternalRequest instance) =>
    <String, dynamic>{
      'externalId': instance.externalId,
      'externalType': instance.externalType,
    };
