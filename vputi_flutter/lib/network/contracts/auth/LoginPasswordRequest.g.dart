// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'LoginPasswordRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginPasswordRequest _$LoginPasswordRequestFromJson(
        Map<String, dynamic> json) =>
    LoginPasswordRequest(
      login: json['login'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$LoginPasswordRequestToJson(
        LoginPasswordRequest instance) =>
    <String, dynamic>{
      'login': instance.login,
      'password': instance.password,
    };
