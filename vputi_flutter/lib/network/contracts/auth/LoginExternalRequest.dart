import 'package:json_annotation/json_annotation.dart';

part 'LoginExternalRequest.g.dart';

@JsonSerializable()
class LoginExternalRequest {
  String externalId;
  String externalType;

  LoginExternalRequest({required this.externalId, required this.externalType});

  factory LoginExternalRequest.fromJson(Map<String, dynamic> json) =>
      _$LoginExternalRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LoginExternalRequestToJson(this);
}
