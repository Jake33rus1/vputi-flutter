import 'package:json_annotation/json_annotation.dart';

part 'AuthTokenResponse.g.dart';

@JsonSerializable()
class AuthTokenResponse {
  String accessToken;
  String refreshToken;

  AuthTokenResponse({required this.accessToken, required this.refreshToken});

  factory AuthTokenResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthTokenResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AuthTokenResponseToJson(this);
}
