import 'package:json_annotation/json_annotation.dart';

part 'RegisterRequest.g.dart';

@JsonSerializable()
class RegisterRequest {
  String login;
  String password;
  String name;
  String surname;
  String phone;
  String email;
  List<int> interestIds;
  Sex sex;

  RegisterRequest(
      {required this.name,
      required this.surname,
      required this.login,
      required this.password,
      required this.phone,
      required this.email,
      required this.interestIds,
      required this.sex});

  factory RegisterRequest.fromJson(Map<String, dynamic> json) =>
      _$RegisterRequestFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterRequestToJson(this);
}

enum Sex { Male, Female }
