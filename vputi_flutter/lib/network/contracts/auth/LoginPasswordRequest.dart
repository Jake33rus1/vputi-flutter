import 'package:json_annotation/json_annotation.dart';

part 'LoginPasswordRequest.g.dart';

@JsonSerializable()
class LoginPasswordRequest {
  String login;
  String password;

  LoginPasswordRequest({required this.login, required this.password});

  factory LoginPasswordRequest.fromJson(Map<String, dynamic> json) =>
      _$LoginPasswordRequestFromJson(json);

  Map<String, dynamic> toJson() => _$LoginPasswordRequestToJson(this);
}
