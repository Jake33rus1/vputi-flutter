// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RegisterRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterRequest _$RegisterRequestFromJson(Map<String, dynamic> json) =>
    RegisterRequest(
      name: json['name'] as String,
      surname: json['surname'] as String,
      login: json['login'] as String,
      password: json['password'] as String,
      phone: json['phone'] as String,
      email: json['email'] as String,
      interestIds:
          (json['interestIds'] as List<dynamic>).map((e) => e as int).toList(),
      sex: $enumDecode(_$SexEnumMap, json['sex']),
    );

Map<String, dynamic> _$RegisterRequestToJson(RegisterRequest instance) =>
    <String, dynamic>{
      'login': instance.login,
      'password': instance.password,
      'name': instance.name,
      'surname': instance.surname,
      'phone': instance.phone,
      'email': instance.email,
      'interestIds': instance.interestIds,
      'sex': _$SexEnumMap[instance.sex],
    };

const _$SexEnumMap = {
  Sex.Male: 'Male',
  Sex.Female: 'Female',
};
