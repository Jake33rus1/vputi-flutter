// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponse _$UserResponseFromJson(Map<String, dynamic> json) => UserResponse(
      fullName: json['fullName'] as String,
      phone: json['phone'] as String,
      interest: (json['interest'] as List<dynamic>)
          .map((e) => UserInterest.fromJson(e as Map<String, dynamic>))
          .toList(),
      settings: Settings.fromJson(json['settings'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserResponseToJson(UserResponse instance) =>
    <String, dynamic>{
      'fullName': instance.fullName,
      'phone': instance.phone,
      'interest': instance.interest,
      'settings': instance.settings,
    };
