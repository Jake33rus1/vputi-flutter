import 'package:json_annotation/json_annotation.dart';

part 'UpdateUserRequest.g.dart';

@JsonSerializable()
class UpdateUserRequest {
  String fullName;
  String phone;
  List<int> interestIds;

  UpdateUserRequest(
      {required this.fullName, required this.phone, required this.interestIds});

  factory UpdateUserRequest.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateUserRequestToJson(this);
}
