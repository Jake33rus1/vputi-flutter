// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserInterest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInterest _$UserInterestFromJson(Map<String, dynamic> json) => UserInterest(
      id: json['id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$UserInterestToJson(UserInterest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
