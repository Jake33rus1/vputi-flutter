// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UpdateUserRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateUserRequest _$UpdateUserRequestFromJson(Map<String, dynamic> json) =>
    UpdateUserRequest(
      fullName: json['fullName'] as String,
      phone: json['phone'] as String,
      interestIds:
          (json['interestIds'] as List<dynamic>).map((e) => e as int).toList(),
    );

Map<String, dynamic> _$UpdateUserRequestToJson(UpdateUserRequest instance) =>
    <String, dynamic>{
      'fullName': instance.fullName,
      'phone': instance.phone,
      'interestIds': instance.interestIds,
    };
