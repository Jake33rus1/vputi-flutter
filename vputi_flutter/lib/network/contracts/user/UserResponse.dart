import 'package:json_annotation/json_annotation.dart';
import 'package:vputi_flutter/network/contracts/user/Settings.dart';

import 'UserInterest.dart';

part 'UserResponse.g.dart';

@JsonSerializable()
class UserResponse {
  String fullName;
  String phone;
  List<UserInterest> interest;
  Settings settings;

  UserResponse(
      {required this.fullName,
      required this.phone,
      required this.interest,
      required this.settings});

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
}
