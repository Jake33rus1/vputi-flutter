import 'package:json_annotation/json_annotation.dart';

part 'Settings.g.dart';

@JsonSerializable()
class Settings {
  bool notifications;
  String language;

  Settings({required this.notifications, required this.language});

  factory Settings.fromJson(Map<String, dynamic> json) =>
      _$SettingsFromJson(json);

  Map<String, dynamic> toJson() => _$SettingsToJson(this);
}
