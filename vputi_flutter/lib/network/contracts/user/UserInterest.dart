import 'package:json_annotation/json_annotation.dart';

part 'UserInterest.g.dart';

@JsonSerializable()
class UserInterest {
  int id;
  String name;

  UserInterest({required this.id, required this.name});

  factory UserInterest.fromJson(Map<String, dynamic> json) =>
      _$UserInterestFromJson(json);

  Map<String, dynamic> toJson() => _$UserInterestToJson(this);
}
