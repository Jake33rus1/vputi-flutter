import 'package:json_annotation/json_annotation.dart';

import 'City.dart';

part 'GetTopCitiesResponse.g.dart';

@JsonSerializable()
class GetTopCitiesResponse {
  List<City> cities;

  GetTopCitiesResponse({required this.cities});

  factory GetTopCitiesResponse.fromJson(Map<String, dynamic> json) =>
      _$GetTopCitiesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetTopCitiesResponseToJson(this);
}
