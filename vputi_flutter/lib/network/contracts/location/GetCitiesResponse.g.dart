// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetCitiesResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCitiesResponse _$GetCitiesResponseFromJson(Map<String, dynamic> json) =>
    GetCitiesResponse(
      cities: (json['cities'] as List<dynamic>)
          .map((e) => City.fromJson(e as Map<String, dynamic>))
          .toList(),
      pageToken: json['pageToken'] as String?,
    );

Map<String, dynamic> _$GetCitiesResponseToJson(GetCitiesResponse instance) =>
    <String, dynamic>{
      'cities': instance.cities,
      'pageToken': instance.pageToken,
    };
