import 'package:json_annotation/json_annotation.dart';

part 'FindCityRequest.g.dart';

@JsonSerializable()
class FindCityRequest {
  String search;
  String? pageToken;

  FindCityRequest({required this.search, required this.pageToken});

  factory FindCityRequest.fromJson(Map<String, dynamic> json) =>
      _$FindCityRequestFromJson(json);

  Map<String, dynamic> toJson() => _$FindCityRequestToJson(this);
}
