// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetTopCitiesResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetTopCitiesResponse _$GetTopCitiesResponseFromJson(
        Map<String, dynamic> json) =>
    GetTopCitiesResponse(
      cities: (json['cities'] as List<dynamic>)
          .map((e) => City.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetTopCitiesResponseToJson(
        GetTopCitiesResponse instance) =>
    <String, dynamic>{
      'cities': instance.cities,
    };
