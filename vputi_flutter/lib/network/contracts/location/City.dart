import 'package:json_annotation/json_annotation.dart';

part 'City.g.dart';

@JsonSerializable()
class City {
  int id;
  String name;
  String imageUrl;

  City({required this.id, required this.name, required this.imageUrl});

  factory City.fromJson(Map<String, dynamic> json) => _$CityFromJson(json);

  Map<String, dynamic> toJson() => _$CityToJson(this);
}
