import 'package:json_annotation/json_annotation.dart';
import 'package:vputi_flutter/network/contracts/location/City.dart';

part 'GetCitiesResponse.g.dart';

@JsonSerializable()
class GetCitiesResponse {
  List<City> cities;
  String? pageToken;

  GetCitiesResponse({required this.cities, required this.pageToken});

  factory GetCitiesResponse.fromJson(Map<String, dynamic> json) =>
      _$GetCitiesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetCitiesResponseToJson(this);
}
