// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FindCityRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FindCityRequest _$FindCityRequestFromJson(Map<String, dynamic> json) =>
    FindCityRequest(
      search: json['search'] as String,
      pageToken: json['pageToken'] as String?,
    );

Map<String, dynamic> _$FindCityRequestToJson(FindCityRequest instance) =>
    <String, dynamic>{
      'search': instance.search,
      'pageToken': instance.pageToken,
    };
