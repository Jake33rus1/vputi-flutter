// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AddCityRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddCityRequest _$AddCityRequestFromJson(Map<String, dynamic> json) =>
    AddCityRequest(
      cityName: json['cityName'] as String,
    );

Map<String, dynamic> _$AddCityRequestToJson(AddCityRequest instance) =>
    <String, dynamic>{
      'cityName': instance.cityName,
    };
