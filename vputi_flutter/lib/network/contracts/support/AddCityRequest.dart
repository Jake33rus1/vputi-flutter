import 'package:json_annotation/json_annotation.dart';

part 'AddCityRequest.g.dart';

@JsonSerializable()
class AddCityRequest{
  String cityName;

  AddCityRequest({required this.cityName});

  factory AddCityRequest.fromJson(Map<String, dynamic> json) =>
      _$AddCityRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AddCityRequestToJson(this);
}