import 'package:json_annotation/json_annotation.dart';

import 'PointLocation.dart';

part 'GetActualRouteResponse.g.dart';

@JsonSerializable()
class GetActualRouteResponse {
  int routeId;
  ShortPointInformation currentPoint;
  ShortPointInformation nextPoint;

  GetActualRouteResponse(
      {required this.routeId,
      required this.currentPoint,
      required this.nextPoint});

  factory GetActualRouteResponse.fromJson(Map<String, dynamic> json) =>
      _$GetActualRouteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetActualRouteResponseToJson(this);
}

@JsonSerializable()
class ShortPointInformation {
  int pointId;
  String name;
  PointLocation location;

  ShortPointInformation(
      {required this.pointId, required this.name, required this.location});

  factory ShortPointInformation.fromJson(Map<String, dynamic> json) =>
      _$ShortPointInformationFromJson(json);

  Map<String, dynamic> toJson() => _$ShortPointInformationToJson(this);
}
