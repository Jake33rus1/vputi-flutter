import 'package:json_annotation/json_annotation.dart';

import 'Route.dart';

part 'GetRouteResponse.g.dart';

@JsonSerializable()
class GetRouteResponse {
  Route route;

  GetRouteResponse({required this.route});

  factory GetRouteResponse.fromJson(Map<String, dynamic> json) =>
      _$GetRouteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetRouteResponseToJson(this);
}
