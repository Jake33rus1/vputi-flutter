// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PointLocation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointLocation _$PointLocationFromJson(Map<String, dynamic> json) =>
    PointLocation(
      longitude: (json['longitude'] as num).toDouble(),
      latitude: (json['latitude'] as num).toDouble(),
    );

Map<String, dynamic> _$PointLocationToJson(PointLocation instance) =>
    <String, dynamic>{
      'longitude': instance.longitude,
      'latitude': instance.latitude,
    };
