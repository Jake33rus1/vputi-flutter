// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetUserRouteResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetUserRouteResponse _$GetUserRouteResponseFromJson(
        Map<String, dynamic> json) =>
    GetUserRouteResponse(
      routes: (json['routes'] as List<dynamic>)
          .map((e) => Route.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetUserRouteResponseToJson(
        GetUserRouteResponse instance) =>
    <String, dynamic>{
      'routes': instance.routes,
    };
