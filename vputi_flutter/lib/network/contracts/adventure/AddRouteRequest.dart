import 'package:json_annotation/json_annotation.dart';

part 'AddRouteRequest.g.dart';

@JsonSerializable()
class AddRouteRequest {
  String start;
  String end;
  List<String> images;
  List<AddRouteRequestPoint> points;

  AddRouteRequest(
      {required this.start,
      required this.end,
      required this.images,
      required this.points});

  factory AddRouteRequest.fromJson(Map<String, dynamic> json) =>
      _$AddRouteRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AddRouteRequestToJson(this);
}

@JsonSerializable()
class AddRouteRequestPoint {
  int pointId;
  String start;
  String end;

  AddRouteRequestPoint(
      {required this.pointId, required this.start, required this.end});

  factory AddRouteRequestPoint.fromJson(Map<String, dynamic> json) =>
      _$AddRouteRequestPointFromJson(json);

  Map<String, dynamic> toJson() => _$AddRouteRequestPointToJson(this);
}
