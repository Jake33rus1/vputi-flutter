// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Point.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Point _$PointFromJson(Map<String, dynamic> json) => Point(
      pointId: json['pointId'] as int,
      pointType: json['pointType'] as int,
      start: json['start'] as String,
      end: json['end'] as String,
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      price: (json['price'] as num).toDouble(),
      title: json['title'] as String,
      shortDescription: json['shortDescription'] as String,
      location:
          PointLocation.fromJson(json['location'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PointToJson(Point instance) => <String, dynamic>{
      'pointId': instance.pointId,
      'pointType': instance.pointType,
      'start': instance.start,
      'end': instance.end,
      'images': instance.images,
      'price': instance.price,
      'title': instance.title,
      'shortDescription': instance.shortDescription,
      'location': instance.location,
    };
