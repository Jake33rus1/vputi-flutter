import 'package:json_annotation/json_annotation.dart';

import 'Point.dart';

part 'Route.g.dart';

@JsonSerializable()
class Route {
  int routeId;
  String start;
  String end;
  double totalPrice;
  List<String> images;
  List<Point> points;

  Route(
      {required this.routeId,
      required this.start,
      required this.end,
      required this.totalPrice,
      required this.images,
      required this.points});

  factory Route.fromJson(Map<String, dynamic> json) => _$RouteFromJson(json);

  Map<String, dynamic> toJson() => _$RouteToJson(this);
}