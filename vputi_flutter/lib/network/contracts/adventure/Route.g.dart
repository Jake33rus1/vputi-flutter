// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Route.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Route _$RouteFromJson(Map<String, dynamic> json) => Route(
      routeId: json['routeId'] as int,
      start: json['start'] as String,
      end: json['end'] as String,
      totalPrice: (json['totalPrice'] as num).toDouble(),
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      points: (json['points'] as List<dynamic>)
          .map((e) => Point.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RouteToJson(Route instance) => <String, dynamic>{
      'routeId': instance.routeId,
      'start': instance.start,
      'end': instance.end,
      'totalPrice': instance.totalPrice,
      'images': instance.images,
      'points': instance.points,
    };
