// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetRouteResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetRouteResponse _$GetRouteResponseFromJson(Map<String, dynamic> json) =>
    GetRouteResponse(
      route: Route.fromJson(json['route'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetRouteResponseToJson(GetRouteResponse instance) =>
    <String, dynamic>{
      'route': instance.route,
    };
