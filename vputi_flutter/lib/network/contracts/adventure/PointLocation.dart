import 'package:json_annotation/json_annotation.dart';

part 'PointLocation.g.dart';

@JsonSerializable()
class PointLocation {
  double longitude;
  double latitude;

  PointLocation({required this.longitude, required this.latitude});

  factory PointLocation.fromJson(Map<String, dynamic> json) =>
      _$PointLocationFromJson(json);

  Map<String, dynamic> toJson() => _$PointLocationToJson(this);
}
