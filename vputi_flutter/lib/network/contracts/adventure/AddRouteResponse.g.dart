// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AddRouteResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddRouteResponse _$AddRouteResponseFromJson(Map<String, dynamic> json) =>
    AddRouteResponse(
      routeId: json['routeId'] as int,
    );

Map<String, dynamic> _$AddRouteResponseToJson(AddRouteResponse instance) =>
    <String, dynamic>{
      'routeId': instance.routeId,
    };
