// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetActualRouteResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetActualRouteResponse _$GetActualRouteResponseFromJson(
        Map<String, dynamic> json) =>
    GetActualRouteResponse(
      routeId: json['routeId'] as int,
      currentPoint: ShortPointInformation.fromJson(
          json['currentPoint'] as Map<String, dynamic>),
      nextPoint: ShortPointInformation.fromJson(
          json['nextPoint'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetActualRouteResponseToJson(
        GetActualRouteResponse instance) =>
    <String, dynamic>{
      'routeId': instance.routeId,
      'currentPoint': instance.currentPoint,
      'nextPoint': instance.nextPoint,
    };

ShortPointInformation _$ShortPointInformationFromJson(
        Map<String, dynamic> json) =>
    ShortPointInformation(
      pointId: json['pointId'] as int,
      name: json['name'] as String,
      location:
          PointLocation.fromJson(json['location'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ShortPointInformationToJson(
        ShortPointInformation instance) =>
    <String, dynamic>{
      'pointId': instance.pointId,
      'name': instance.name,
      'location': instance.location,
    };
