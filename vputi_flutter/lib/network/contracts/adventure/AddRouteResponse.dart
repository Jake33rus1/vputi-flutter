import 'package:json_annotation/json_annotation.dart';

part 'AddRouteResponse.g.dart';

@JsonSerializable()
class AddRouteResponse {
  int routeId;

  AddRouteResponse({required this.routeId});

  factory AddRouteResponse.fromJson(Map<String, dynamic> json) =>
      _$AddRouteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AddRouteResponseToJson(this);
}
