import 'package:json_annotation/json_annotation.dart';

import 'Route.dart';

part 'GetUserRouteResponse.g.dart';

@JsonSerializable()
class GetUserRouteResponse {
  List<Route> routes;

  GetUserRouteResponse({required this.routes});

  factory GetUserRouteResponse.fromJson(Map<String, dynamic> json) =>
      _$GetUserRouteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetUserRouteResponseToJson(this);
}
