import 'package:json_annotation/json_annotation.dart';

import 'PointLocation.dart';

part 'Point.g.dart';

@JsonSerializable()
class Point {
  int pointId;
  int pointType;
  String start;
  String end;
  List<String> images;
  double price;
  String title;
  String shortDescription;
  PointLocation location;

  Point(
      {required this.pointId,
        required this.pointType,
        required this.start,
        required this.end,
        required this.images,
        required this.price,
        required this.title,
        required this.shortDescription,
        required this.location});

  factory Point.fromJson(Map<String, dynamic> json) => _$PointFromJson(json);

  Map<String, dynamic> toJson() => _$PointToJson(this);
}