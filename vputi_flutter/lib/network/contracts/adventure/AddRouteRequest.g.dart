// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AddRouteRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddRouteRequest _$AddRouteRequestFromJson(Map<String, dynamic> json) =>
    AddRouteRequest(
      start: json['start'] as String,
      end: json['end'] as String,
      images:
          (json['images'] as List<dynamic>).map((e) => e as String).toList(),
      points: (json['points'] as List<dynamic>)
          .map((e) => AddRouteRequestPoint.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AddRouteRequestToJson(AddRouteRequest instance) =>
    <String, dynamic>{
      'start': instance.start,
      'end': instance.end,
      'images': instance.images,
      'points': instance.points,
    };

AddRouteRequestPoint _$AddRouteRequestPointFromJson(
        Map<String, dynamic> json) =>
    AddRouteRequestPoint(
      pointId: json['pointId'] as int,
      start: json['start'] as String,
      end: json['end'] as String,
    );

Map<String, dynamic> _$AddRouteRequestPointToJson(
        AddRouteRequestPoint instance) =>
    <String, dynamic>{
      'pointId': instance.pointId,
      'start': instance.start,
      'end': instance.end,
    };
