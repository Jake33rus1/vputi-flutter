import 'package:dio/dio.dart';
import 'package:grpc/grpc.dart';
import 'package:vputi_flutter/network/AdventureApi.dart';
import 'package:vputi_flutter/network/AuthApi.dart';
import 'package:vputi_flutter/network/InterestsApi.dart';
import 'package:vputi_flutter/network/LocationApi.dart';
import 'package:vputi_flutter/network/SupportApi.dart';
import 'package:vputi_flutter/network/UserApi.dart';
import 'package:vputi_flutter/src/generated/route.pbgrpc.dart';

class ApiFactoryConfig {
  static String AuthApiUrl = "http://172.28.192.1:5003";
  static String InterestsApiUrl = "$AuthApiUrl/interests";
  static String AdventurteApiUrl = "$AuthApiUrl/adventure";
  static String SupportApiUrl = "$AuthApiUrl/support";
  static String LocationApiUrl = "$AuthApiUrl/location";
  static String UserApiUrl = "$AuthApiUrl/user";
  static String AdventurteGrpcApiUrl = AuthApiUrl;
}

class ApiFactory {
  static AuthApi createAuthApi() {
    final dio = Dio();
    return AuthApi(dio, baseUrl: ApiFactoryConfig.AuthApiUrl);
  }

  static AdventureApi createAdventureApi() {
    final dio = Dio();
    return AdventureApi(dio, baseUrl: ApiFactoryConfig.AdventurteApiUrl);
  }

  static InterestsApi createInterestsApi() {
    final dio = Dio();
    return InterestsApi(dio, baseUrl: ApiFactoryConfig.InterestsApiUrl);
  }

  static RouteServiceClient createAdventureGrpcApi() {
    const credentials = ChannelCredentials.insecure();
    const channelOptions = ChannelOptions(credentials: credentials);

    final channel = ClientChannel(
      ApiFactoryConfig.AdventurteGrpcApiUrl,
      options: channelOptions,
    );

    return RouteServiceClient(channel);
  }

  static LocationApi createLocationApi() {
    final dio = Dio();
    return LocationApi(dio, baseUrl: ApiFactoryConfig.LocationApiUrl);
  }

  static SupportApi createSupportApi() {
    final dio = Dio();
    return SupportApi(dio, baseUrl: ApiFactoryConfig.SupportApiUrl);
  }

  static UserApi createUserApi() {
    final dio = Dio();
    return UserApi(dio, baseUrl: ApiFactoryConfig.UserApiUrl);
  }
}
