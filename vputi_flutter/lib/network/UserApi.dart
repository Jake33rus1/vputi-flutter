import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import 'contracts/user/UpdateUserRequest.dart';
import 'contracts/user/UserResponse.dart';

part 'UserApi.g.dart';

@RestApi()
abstract class UserApi {
  factory UserApi(Dio dio, {String baseUrl}) = _UserApi;

  @GET("/")
  Future<UserResponse> getUser();

  @POST("/")
  Future updateUser(@Body() UpdateUserRequest request);
}
