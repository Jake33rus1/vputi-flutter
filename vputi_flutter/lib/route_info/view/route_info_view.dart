import 'package:flutter/material.dart';

class RouteInfoView extends StatefulWidget {
  @override
  _RouteInfoViewState createState() => _RouteInfoViewState();
}

class _RouteInfoViewState extends State<RouteInfoView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      top: false,
      bottom: true,
      child: Column(children: [
        Container(
            height: MediaQuery.of(context).size.height * 0.8,
            child: Stack(
              children: [
                Positioned(
                  child: Image(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.5,
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        "https://images.unsplash.com/photo-1604612503444-4b6e7fc9964d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1738&q=80"),
                  ),
                ),
                Positioned(
                    top: MediaQuery.of(context).size.height * 0.35,
                    left: MediaQuery.of(context).size.width * 0.1,
                    child: RichText(
                      text: TextSpan(
                          text: "Карелия",
                          style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)),
                    )),
                Positioned(
                    top: MediaQuery.of(context).size.height * 0.40,
                    left: MediaQuery.of(context).size.width * 0.1,
                    child: RichText(
                      text: TextSpan(
                          text: "Медвежьегорск-Петрозаводск",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Colors.white)),
                    )),
                Positioned(
                    top: MediaQuery.of(context).size.height * 0.45,
                    left: MediaQuery.of(context).size.width * 0.1,
                    right: MediaQuery.of(context).size.width * 0.1,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.width * 0.7,
                      child: Card(
                        elevation: 6,
                        color: Colors.white,
                        child: Padding(
                          padding: EdgeInsets.all(16.0),
                          child: RichText(
                              text: TextSpan(
                                  text: "Текущая точка маршрута",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold))),
                        ),
                      ),
                    ))
              ],
            )),
      ]),
    ));
  }
}
